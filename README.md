# Test Coverage Reporting

Testing how to report test coverage obtained with `pytest` as a badge.


## Steps taken

 1. Add the Link variable under `Settings > Badges` (just the destination when clicking the image, i.e. the badge)
    ```
    https://gitlab.com/%{project_path}
    ```
 2. Add path to badge at the same location
    ```
    https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg
    ```
    Currently, a badge with unknown status should be displayed
 3. Add the regex phrase to the `Test coverage parsing` under `Settings > CI/CD > General 
    Pipelines`
    ```
    ^TOTAL.+?(\d+\%)$
    ```
 4. _optional:_ if the coverage reported from a specific job should be reported, the 
    `job` and the badge's `key text` can be specified. In the following, the coverage 
    of unit tests is reported
    ```
    https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg?job=unit-tests&key_text=Unit+Tests
    ```
    

## Final Result 

![img.png](resources/img.png)
