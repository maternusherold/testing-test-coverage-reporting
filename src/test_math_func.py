#!/usr/bin/env python3


"""Unit tests for math_func.py functions. """


import math_func
import pytest


def helper():
    print("helped")


def test_add():
    # assert math_func.addition(4, 4) == 10
    assert math_func.addition(4, 4) == 8


# using the decorator to run all test cases regardles of the prev. results
# allows to also test even if one fails
@pytest.mark.parametrize("test_input, expected", [([4, 4], 16), ([5, 5], 25)])
def test_multiplication(test_input, expected):
    assert math_func.multiplication(*test_input) == expected
